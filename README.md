# README #

##WEB GUI SETUP##
This must be done on the ugrad servers!

1. Make sure our server is offline.

    ps -fA | grep python

    find the PID (first number) of the process running on port 22

    kill -9 *the PID*

2. Run server on your machine, suppressing all output

    nohup python server.py 22 > & 1 &


3. Open http://ugradx.cs.jhu.edu:20022

4. ./main and you are good to go!

### TODO ###

I just noticed that the loser of the round does not always go first. I added a field to player (bool goes_first) and a few methods to fix this, but it isn't working completely. -GG


Extensive testing! As far as I can see all the moves work, but please check for edge cases. Let me know of any weird behavior! - TC

### NOTES: ###
The main game loop now has crude quit options to end the game neatly while we're doing our testing. I also fixed valgrind so we have no memory leaks! - TC

I'm not sure where else this might come up, so here's how I fixed input errors:

To fix any input errors (from user entering something other than a number, not an out of bounds number):

Instead of "cin >> variable;"
Write "if(!(cin >> variable)) { cin.clear(); cin.ignore(); cout << "Input not valid" << endl; return 0;}
This just fixed cin so there are no infinite loops, then aborts the function.



____________________________________________________________

AI Notes:
If we can use these to prioritize/weight the moves, I gathered some general notes to indicate the best time when moves should be made.

Take several
- If there are multiple diamonds take them
- If there are multiple golds take them
- If there are multiple silvers take them
- If you can make 3 leathers (3 from the market, 1 in hand and 2 from market)

Take 1
- If gold or diamond shows up, take it
- If early in game, or you already have some, take one silver
- Take one leather if your hand is almost full to slow your opponent's gathering of them

Take camels
- If they have a full (7 card) hand
- If they have a small hand and very few camels (1 or 2)
- If near end of game and taking the camels will give you the largest herd

Sell
- Sell leather as soon as possible, early in the game
- Sell spice and cloth next