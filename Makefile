CFLAGS = -std=c++11 -pedantic -Wall -Wextra -O
LFAGS =
CC = g++
GCOV = -fprofile-arcs -ftest-coverage








clean:
	\rm -f *.o *.out *.swp *.toc *.aux *.log *.odf
