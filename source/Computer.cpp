#include "Computer.h"

#include <string>
#include <iostream>
#include <exception>
#include <algorithm>
#include <ctype.h>
#include <cstdlib>
#include <ctime>

using std:: string;


Computer::Computer(string s) : Player(s) {}

int Computer::makeMove(Board * b) {

    // 'a' is take, 'b' is sell
    char choice;

    // if have camels or have room in hand, take
    if ((b->numCamels() > 0) || (hand()->size() < 7)) {
        choice = 'a';
    }
    // else if can sell
    else if (hand()->size() == 7) {
        choice = 'b';
    }
    // else, take
    else {
        choice = 'a';
    }
    
    return makeMove(choice, b);
}


int Computer::makeMove(char choice, Board * b) {

    // a is take
    if (choice == 'a') {
        // take camels
        if (b->numCamels() > 0) {
            return takeC(b);
        }
        // else take one if hand not almost full
        else if (hand()->size() < 7) {
            return takeA(b);
        }
        // else if have cheap goods, take several
        else if (hand()->at(0).getType() < 4) {
            return takeB(b);
        }
    }
    // b is sell
    else if (choice == 'b') {
        return sellChoices(b);
    }

    return 0;

}

// take first card
int Computer::takeA(Board * b) {
    // generate random position from 1 to 5
    std::srand(std::time(0));
    int position = std::rand() % 5 + 1;
    Take t = Take(b, this);

    int i, j;
    // take a card that matches, or take random
    for (i = (signed) hand()->size() - 1; i >= 0; i--) {
        for (j = 0; j < (signed) b->market()->size(); j++) {
            if (hand()->at(i).getType() == b->market()->at(j).getType()) {
                return t.takeOne(j + 1);
            }
        }
    }

    return t.takeOne(position);
}

// take highest of same type
int Computer::takeB(Board * b) {

    int i;
    vector<int> toGive;
    vector<int> toTake;

    // if have several singles, toGive
    for (i = 0; i < (signed) hand()->size()-1; i++) {
        if (hand()->at(i).getType() != hand()->at(i+1).getType()) {
            toGive.push_back(i + 1);
        }
    }
    // take that many from market
    for (i = 0; i < (signed) toGive.size(); i++) {
        toTake.push_back(i + 1);
    }

    Take t = Take(b, this);
    return t.takeSeveral(toTake, toGive, 0);
}

int Computer::takeC(Board * b) {

    Take t = Take(b, this);
    return t.takeCamels();
}

int Computer::sellChoices(Board * b) {
    vector<int> toSell;
    
    Sell S(b, this);

    // if can sell multiple goods
    int i, count = 0, highest = 0, hightype = 0;
    for (i = 0; i < (signed) hand()->size()-1; i++) {
        if (hand()->at(i).getType() == hand()->at(i+1).getType()) count++;
        else {
            if (count) count++;
            if (highest < count) {
                highest = count;
                hightype = hand()->at(i).getType();
            }
            count = 0;
        }
    }
    // for last one
    if (count) count++;
    if (highest < count) {
        highest = count;
        hightype = hand()->at(i).getType();
    }

    // if have 2+ goods of same type, sell
    if (highest > 2) {
        for (i = 0; i < (signed) hand()->size(); i++) {
            if (hand()->at(i).getType() == hightype) {
                toSell.push_back(i + 1);
            }
        }
        return S.sell(toSell);
    }

    // generate random position from hand
    std::srand(std::time(0));
    int position = (std::rand() % (hand()->size() - 1)) + 1;
    // sell card of lowest value, trying to avoid 2+ expensive goods rule
    int t = hand()->at(position).getType();
    // if it's an expensive good
    if (t > 3) {
        toSell = {1};
    }
    else {
        toSell = {position};
    }

    return S.sell(toSell);
}
