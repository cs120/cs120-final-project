#ifndef SELL_H
#define SELL_H

#include <vector>
#include <string>
#include <iostream>

using std::vector;
using std::string;


class Sell : public Move {

public:

    Sell(Board *, Player *);
    ~Sell() {}

    //sell cards in positions from hand
    int sell(vector<int>);
    //check if a sell is valid or not
    bool validSell(vector<int>);


};


#endif

