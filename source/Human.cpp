#include "Human.h"
#include <string>
#include <iostream>
#include <exception>
#include <algorithm>
#include <ctype.h>

using std:: string;
using std::cout;
using std::endl;
using std::cin;
using namespace Type;


Human::Human(string s) : Player(s) {}

int Human::makeMove(Board * b) {

	char choice;

	cout << "Would you like to take cards (a), or sell cards (b)? ";
	cin >> choice;
	
	return makeMove(choice, b);

}

int Human::makeMove(char choice, Board * b) {
    char move;

    // a is take
    if (choice == 'a') {

        // prompt for take option
        cout << "What would you like to do: " << endl <<
                "(a) Take 1 Card" << endl <<
                "(b) Take Multiple Cards" << endl <<
                "(c) Take Camels" << endl;
        cout << ">>> ";
        cin >> move;
        if (move == 'a') {
            return takeA(b);
        }
        else if (move == 'b') {
            return takeB(b);
        }
        else if (move  == 'c') {
            return takeC(b);
        }
        else {
            cout << "INVALID: Invalid choice." << endl;
            return 0;
        }
    }
    // b is sell
    else if (choice == 'b') {
        // check if can sell
        return sellChoices(b);
    }

    cout << "INVALID: Invalid option." << endl;

    return 0;
}

int Human::sellChoices(Board * b) {
    vector<int> toSell;
    int choice = 1;
    // prompt for which cards to sell
    cout << "Enter cards to sell, each on a separate line."
     << endl << "Enter 0 to stop." << endl;
    while (choice != 0) {
        cout << ">>> ";
        if(!(cin >> choice)) {cin.clear(); cin.ignore(); cout << "Not valid input" << endl; return 0;}
        if (choice) {
       	    toSell.push_back(choice);
        }
    }
	Sell S(b, this);
	return S.sell(toSell);
}

// option A: take 1 card, replace with draw
int Human::takeA(Board * b) {
    int position;

    // prompt which card to take
    cout << "Which card to take: " << endl;
    cout << ">>> ";
    if(!(cin >> position)){
        cin.clear();
        cin.ignore();
        cout << "Not valid input" << endl;
        return 0;
    }


    Take t = Take(b, this);
    return t.takeOne(position);
}

// option B: take multiple cards, replace from hand
int Human::takeB(Board * b) {
    int position, numTake, numCards, i;
    numCards = 0;

    for(i=0; i<5; i++) {
        numCards += (b->market()->at(i).getType() != Ca);
    }

    vector<int> choicesIn;
    vector<int> choicesOut;

    // if only 2 non-camel cards available
    if (numCards == 2) {
        cout << "Only two goods available. Both will be taken." << endl;
        numTake = 2;
        for (i = 0; i < 5; i++) {   
            if (b->market()->at(i).getType() != Ca) {
                choicesIn.push_back(i + 1);
                cout << typenames[b->market()->at(i).getType()] << " was taken." << endl;
            }
        }
        cout << endl;
    }
    // if more than 3 non-camel cards available
    else {
        cout << "How many would you like to take?" << endl;
        cout << "Enter a number between 2 and "
        << numCards << " inclusive: " << endl;
        cout << ">>> ";
        if(!(cin >> numTake)) {cin.clear(); cin.ignore(); cout << "Not valid input" << endl; return 0;}

        // validate input
        if(numTake < 2 || numTake > numCards) {
            cout << "INVALID: Cannot take " << numTake << ", only " << numCards <<  " in market." << endl;
            return 0;
        }

        cout << "Choose " << numTake << " cards to take." << endl;
        for (i = 1; i <= numTake; i++) {   
            cout << ">>> ";

            if(!(cin >> position)) {
                cin.clear();
                cin.ignore();
                cout << "Not valid input" << endl;
                return 0;
            }

            if(position > (signed) b->market()->size() || position < 1) {
                cout << "INVALID: Invalid position." << position << endl;
                return 0;
            }
            if(b->market()->at(position - 1).getType() == Ca) {
                cout << "INVALID: You can't take camels! Only goods!" << endl;
                i--;
            } else if (find(choicesIn.begin(), choicesIn.end(), position) != choicesIn.end()) {
                cout << "INVALID: You can't take the same card twice!" << endl;
                i--;
            }
            else {
                choicesIn.push_back(position);
            }
        }
    }
    
    // ask for cards to give back
    cout << "Choose " << choicesIn.size() << " cards to give back." << endl;
    cout << "HINT: Goods and camels are valid. Choose (0) to use Camel." << endl;
    for (i = 0; i < (signed) choicesIn.size(); i++) {
        cout << ">>> ";
        if(!(cin >> position)) {
            cin.clear();
            cin.ignore(); 
            cout << "ERROR: Invalid input." << endl; 
            return 0;
        }
        if(position > (signed) hand()->size() || position < 0) {
            cout << "INVALID: Invalid position." << endl;
            return 0;
        }
        else {
            choicesOut.push_back(position);
        }
    }

    // c is how many camels from herd player wants to use
    int c = 0;
    for(int i = 0; i < (signed) choicesOut.size(); i++) {
        c += (choicesOut.at(i) == 0);
    }

    // remove camels from choicesOut
    choicesOut.erase(remove(choicesOut.begin(), choicesOut.end(), 0), choicesOut.end());

    Take t = Take(b, this);
    return t.takeSeveral(choicesIn, choicesOut, c);
}

// option C: take all camels, replace with draw
int Human::takeC(Board * b) {

    if(b->numCamels() == 0){ 
        cout << "ERROR: No camels to take!" << endl;
        return 0;
    }

    Take t = Take(b, this);
    return t.takeCamels();
}
