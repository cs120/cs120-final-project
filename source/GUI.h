#ifndef GUI_H
#define GUI_H
#include "Board.h"
#include <iostream>
#include <fstream>
#include <string>

class GUI {
protected:
    Board * board;
    std::ofstream * fp;
private:
public:

    GUI();
    ~GUI() {
        delete fp;
    };

    void open();
    void setBoard(Board *);
    void update(Player *);

    void drawBoard(std::ofstream *, Player *);
    void drawMarket(std::ofstream *);
    void drawTokens(std::ofstream *);
    void drawHand(std::ofstream *, Player *);

};
#endif
