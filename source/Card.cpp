
#include "Card.h"
#include "Piece.cpp"

#include <string>
#include <functional>

using std::string;

Card::Card(int t) : Piece(t) {} 

//Return order in which types should appear in hand
int Card::rank() const {
    // Greatest to least: Ca, Di, Au, Ag, Cl, Sp, L, NOTYPE
    return type;
}


bool Card::operator< (const Card & other) const {
    return this->rank() < other.rank();
}
