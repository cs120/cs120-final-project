
/* Computer extends the Player class
 * The difference between a human player
 * is that moves are generated rather than selected.
 * The function that accomplishes this is makeMove()
 */

#ifndef COMPUTER_H
#define COMPUTER_H

#include "Player.h"
#include <string>
#include <iostream>

class Computer : public Player {

public:

    Computer(string);

    ~Computer() {}

    int makeMove(Board *);
    int makeMove(char, Board *);

    int takeA(Board *);
    int takeB(Board *);
    int takeC(Board *);

    int sellChoices(Board *);

};



#endif
