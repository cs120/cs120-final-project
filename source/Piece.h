
/*
 * Abstract base class.
 * No piece objects should be created
 */

#ifndef PIECE_H
#define PIECE_H

#include <string>

using std::string;

namespace Type {
    enum {NOTYPE, Le, Sp, Cl, Ag, Au, Di, Bo3, Bo4, Bo5, Ca};
    const string typeabvs[11] = {"No", "Le", "Sp", "Cl", "Ag", "Au", "Di", "B3", "B4", "B5", "Ca"};
    const string typenames[11] = {"NONE", "Leather", "Spice", "Cloth", "Silver", "Gold",
                                "Diamond", "Bonus 3", "Bonus 4", "Bonus 5", "Camel"};
}

class Piece {

protected:
    int type;

public:
    Piece(int);

    virtual ~Piece() {}

    inline int getType() { return type; }

};


#endif
