#ifndef TAKE_H
#define TAKE_H

#include "Move.cpp"

#include <vector>
#include <string>
#include <iostream>

using std::vector;
using std::string;


class Take : public Move {

public:

    Take(Board *, Player *);
    ~Take() {}

    bool isValidA(int);
    bool isValidB(vector<int>, vector<int>, int);

    //Take 1 card 
    int takeOne(int);                

    //Take several
    //param 1 = vector of positions in market of cards to take
    //param 2 = vector of positions in hand of cards to take (with camels removed)
    //param 3 = number of camels to give from herd
    int takeSeveral(vector<int>, vector<int>, int);

    //Take all camels
    //param is a flag so we know to take all camels
    int takeCamels();


};


#endif
