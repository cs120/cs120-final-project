

#include "Token.h"

#include <string>
#include <iostream>

using std::string;
using namespace Type;

//Constructors
Token::Token() : Piece(NOTYPE), value(-1) {}

Token::Token(int t, int i) : Piece(t), value(i) {}


int Token::getValue() {
    return value;
}
