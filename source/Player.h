
/* Abstract player class 
 * contains fields for all major game fields
 * (number of camels, cards, tokens, etc)
 * Extended by Human and Computer to add move execution
 * Player should not be constructed
 */

#ifndef PLAYER_H
#define PLAYER_H

#include "Board.h"
#include "Card.h"
#include "Token.h"
#include <string>
#include <vector>

class Board;
using std::string;
using std::vector;

class Player {

    friend class Board;

    string name;

    vector<Card> theHand;

    vector<Token> tokens;  //tokens bought by player

    int num_points;
    int num_camels; //used to see who gets camel token at end of round
    int num_cards;  //must have 7 or less at end of each turn
    int num_bonus;  //used in case of a tie
    int num_goods;  //used in case there's still a tie

    int num_seals;  //first player with 2 seals wins
    

public:

    Player() {}

    Player(string); //initialize all fields to 0

    virtual ~Player() {}

    //Accessors
    string getName() const;
    int getPoints() const;
    int getCamels() const;
    vector<Card> * hand();

    void addCard(Card);
    void removeCard(int);
    void addToken(Token);

    inline int numCamels() const { return num_camels; }
    inline int numTokens() const { return tokens.size(); }
    
    inline int getBonusTok() const { return num_bonus; }
    inline int getGoodsTok() const { return num_goods; }


    void playCamel();
    void printHand();

    virtual int makeMove(Board *) = 0;
    virtual int makeMove(char, Board *) = 0;

    virtual int takeA(Board *) = 0;
    virtual int takeB(Board *) = 0;
    virtual int takeC(Board *) = 0;

    virtual int sellChoices(Board *) = 0;
    
    
};



#endif
