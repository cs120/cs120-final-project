/*
 * Each game has two players and a board.
 * Game is responsible for controlling interactions between
 * the players and the board objects.
 *
 */

#ifndef GAME_H
#define GAME_H

#include "GUI.h"
#include "Board.h"
#include "Player.h"
#include "Human.h"
#include "Computer.h"
#include "Take.h"
#include "Sell.h"

class Game {

protected:
    Player * p1;
    Player * p2;
	GUI * gui;

    Board b;
    
    int p1Wins = 0;
    int p2Wins = 0;
    

public:

	Game();

    ~Game() {
		delete p1;
		delete p2;
        delete gui;
	}

    Player * initPlayer(char, string);
    Human * initHuman(string);
    Computer * initComputer(string);

    //Accessors
    Player * player1();
    Player * player2();
    Board * board();

    //Setters
    void setPlayer1(char, string);
    void setPlayer2(char, string);
    
    //Determine if game is finished
    bool gameFinished();
    
    //Print round/game winner and increment appropriate win counter
	string determineRoundWinner();
	string determineGameWinner();
    string tieBreaker();
	
	//Start new round
	void newRound();

    //force quit, can use for valgrind
    void forceQuitGame();

	// GUI interface
	void openGUI();
	void updateHTML(Player *);
	
};


#endif
