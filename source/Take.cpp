#include "Take.h"

#include <string>
#include <iostream>
#include <exception>
#include <algorithm>

using std:: string;
using std::cout;
using std::endl;
using std::cin;
using namespace Type;


Take::Take(Board * b, Player * p) : Move(b, p) { }

bool Take::isValidA(int pos) {

    //check if hand is full
    if (player->hand()->size() >= 7) {
        cout << "INVALID: You have a full hand, can't take." << endl;
        return false;
    }

    //check if input valid
    if (pos < 1 || pos > 5) {
        cout << "INVALID: Position invalid." << endl;
        return 0;
    }

    // if camel, prompt to take good, or all camels
    if (board->market()->at(pos-1).getType() == Ca) {
        cout << "INVALID: You can only take goods. To take camels, choose 'Take Camels' option." << endl;
        return false;
    }

    return true;
}



int Take::takeOne(int i) {

    int position = i;

    if (!isValidA(i)) {
        return 0;
    }

    // move a card from market to hand
    board->giveCardFromMarket(player, position);
    // add card from deck to market
    board->addToMarket(board->draw());

    return 1;
}

bool Take::isValidB(vector<int> cardsToTake, vector<int> cardsToGive, int camelsToGive) {

    int numCards = 0, i;

    // get number of goods in market
    for(i=0; i<5; i++) {
        numCards += (board->market()->at(i).getType() != Ca);
    }

    // check if several goods in market
    if(numCards < 2) {
        cout << "INVALID: Not enough cards in market to take several." << endl;
        return false;
    }

    // check if player has enough camels in herd
    if (player->getCamels() < camelsToGive) {
        cout << "INVALID: Not enough camels in herd." << endl;
        return false;
    }

    // check if taking camel
    for (i = 0; i < (signed) cardsToTake.size(); i++) {
        if(board->market()->at(cardsToTake.at(i) - 1).getType() == Ca) {
            cout << "INVALID: You can't take camels! Only goods!" << endl;
            return false;
        }
    }

    // check if taking more than replacing overflows hand
    if ((player->hand()->size() + cardsToTake.size() - cardsToGive.size()) > 7) {
        cout << "INVALID: Taking too many cards." << endl;
        return false;
    }
    
    // if(camelsToGive > board->numCamels()) {
    if(camelsToGive > player->getCamels()) {
        cout << "INVALID: Invalid number of camels." << endl;
        return false;
    }


    // check if cards to give back are of same type as taken
/*    sort(cardsToTake.begin(), cardsToTake.end());
    cardsToTake.erase(unique(cardsToTake.begin(), cardsToTake.end()), cardsToTake.end());

    sort(cardsToGive.begin(), cardsToGive.end());
    cardsToGive.erase(unique(cardsToGive.begin(), cardsToGive.end()), cardsToGive.end());

    // compare
    for (i = 0; i < (signed) cardsToTake.size() - 1; i++) {
        for (int j = 0; j < (signed) cardsToGive.size() - 1; j++) {
            if (cardsToTake.at(i) == cardsToGive.at(j)) {
                cout << "INVALID: Cannot replace goods of same type as taken." << endl;
                return false;
            }
        }
    }*/

    return true;

}


int Take::takeSeveral(vector<int> cardsToTake, vector<int> cardsToGive, int camelsToGive) {

    if (!isValidB(cardsToTake, cardsToGive,camelsToGive)) {
        return 0;
    }

    int i;

    // sort in reverse order so it doesn't mess up positions in hand
    sort(cardsToTake.begin(), cardsToTake.end(), std::greater<int>());
    sort(cardsToGive.begin(), cardsToGive.end(), std::greater<int>());

    // move cards from hand to market
    for (i = 0; i < (signed) cardsToGive.size(); i++) {
        board->giveCardToMarket(player, cardsToGive.at(i));
    }

    // put the cards from the market to player's hand
    for (i = 0; i < (signed) cardsToTake.size(); i++) {
        board->giveCardFromMarket(player, cardsToTake.at(i));
    }

    // move camels from herd to market
    for (i = 0; i < camelsToGive; i++) {
        player->playCamel();
        board->addToMarket(Card(Ca));
    }    

    return 1;

}


int Take::takeCamels() {

    int i;

    // validity
    if(board->numCamels() == 0) {
        cout << "INVALID: No camels in the market." << endl;
        return 0;
    }

    // take camels from market to herd
    for(i=4; i>=0; i--) {
        if((board->market()->at(i).getType() == Ca)) {
            board->giveCardFromMarket(player, i+1);
            board->addToMarket(board->draw()); 
        }
    }

    return 1;
} 







