
/*
 * Card class. Extends Piece.
 * Cards add a value field to hold the rupee value.
 * Cards also overload the < operator for sorting in hand.
 */

#ifndef CARD_H
#define CARD_H

#include "Piece.h"

#include <string>

using std::string;

class Card : public Piece {

public:

    //Create card with given type
    Card(int); 
    //default
    ~Card() {}

    //Overloaded < operator
    bool operator< (const Card &) const;


private:

    int rank() const; //used for ordering cards in hand

};



#endif
