#include "Board.h"
#include "Token.cpp"
#include "Card.cpp"

#include <iostream>
#include <random>
#include <algorithm>
#include <chrono>
#include <ctime>
#include <cstdlib>
#include <exception>

using std::cin;
using std::cout;
using std::endl;
using std::string;
using namespace Type;


Board::Board() {
    
    cout << "Initializing Board ... " << endl;

    initTokens();
    initPile();
}


void Board::initTokens() {

    // cout << "Initializing Tokens ..." << endl;

    int i = 0;
    for(i=0; i<=Bo5; i++)
        tokens.push_back(vector<Token>());

    //Diamond
    for (i=0; i<3; i++) { tokens[Di].push_back(Token(Di, 5)); }
    for (i=0; i<2; i++) { tokens[Di].push_back(Token(Di, 7)); }

    //Gold
    for (i=0; i<3; i++) { tokens[Au].push_back(Token(Au, 5)); }
    for (i=0; i<2; i++) { tokens[Au].push_back(Token(Au, 6)); }

    //Silver
    for (i=0; i<5; i++) { tokens[Ag].push_back(Token(Ag, 5)); }

    //Cloth
    for (i=0; i<2; i++) { tokens[Cl].push_back(Token(Cl, 1)); }
    for (i=0; i<2; i++) { tokens[Cl].push_back(Token(Cl, 2)); }
    for (i=0; i<2; i++) { tokens[Cl].push_back(Token(Cl, 3)); }
                          tokens[Cl].push_back(Token(Cl, 5)); 

    //Spice
    for (i=0; i<2; i++) { tokens[Sp].push_back(Token(Sp, 1)); }
    for (i=0; i<2; i++) { tokens[Sp].push_back(Token(Sp, 2)); }
    for (i=0; i<2; i++) { tokens[Sp].push_back(Token(Sp, 3)); }
                          tokens[Sp].push_back(Token(Sp, 5)); 

    //Leather
    for (i=0; i<6; i++) { tokens[Le].push_back(Token(Le, 1)); }
    for (i=2; i<5; i++) { tokens[Le].push_back(Token(Le, i)); }

    //Bonus
    for (i=0; i<6; i++) { tokens[Bo3].push_back(Token(Bo3, i/2+1)); } // 1 1 2 2 3 3 
    for (i=0; i<6; i++) { tokens[Bo4].push_back(Token(Bo4, i/2+4)); } // 4 4 5 5 6 6 
    for (i=0; i<6; i++) { tokens[Bo5].push_back(Token(Bo5, i/2+8)); } // 8 8 9 9 10 10
    
    std::srand(std::time(0));   
    for(i=Le; i<=Bo5; i++) { std::random_shuffle(tokens[i].begin(), tokens[i].end());}

}


void Board::initPile() {

    // cout << "Initializing pile ..." << endl;

    int i;

    for (i=0; i<6; i++)  { thePile.push_back(Card(Di)); }
    for (i=0; i<6; i++)  { thePile.push_back(Card(Au)); }
    for (i=0; i<6; i++)  { thePile.push_back(Card(Ag)); }
    for (i=0; i<8; i++)  { thePile.push_back(Card(Cl)); }
    for (i=0; i<8; i++)  { thePile.push_back(Card(Sp)); }
    for (i=0; i<10; i++) { thePile.push_back(Card(Le)); }
    for (i=0; i<8; i++)  { thePile.push_back(Card(Ca)); }


    //Shuffle

    //For different random each time
    unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
    std::shuffle (thePile.begin(), thePile.end(), std::default_random_engine(seed));

    //For same order each time you run program
    // std::srand(std::time(0));
    // std::random_shuffle(thePile.begin(), thePile.end());
}



vector<Card> * Board::pile() {
    return &thePile;
}

vector<Card> *  Board::market() {
    return &theMarket;
}



void Board::printMarket() {

    int i;

    cout << "Market: ";

    for (i=0; i< (signed) market()->size(); i++) {
        cout << market()->at(i).getType() << " ";
    }

    cout << endl;
}


void Board::addToMarket(Card c) {
    theMarket.push_back(c);

}    


Card Board::draw() {

    if (thePile.size() <= 0) {
        throw std::range_error("ERROR: Empty deck.");
    }
    
    Card temp = thePile.back();
    thePile.pop_back();
    return temp;
}


void Board::deal(Player * p1, Player * p2) {

    int i;

    cout << "Dealing Cards ..." << endl;

    // Initally add 3 camels to market
    for (i=0; i<3; i++) {
        addToMarket(Card(Ca));
    }
    
    // Put 5 cards in each players hand from pile
    for (i=0; i<5; i++) {
        p1->addCard(draw());
        p2->addCard(draw());
    }

    // Put 2 cards in the market from pile
    for (i=0; i<2; i++) {
        addToMarket(draw());
    }
}

template<typename T>
bool isEmpty(vector<T> v) {
    return v.size() == 0;
}


void Board::giveToken(Player * p, int type, int num) {
    for(int i=0; i<num; i++)
    {
        if (!isEmpty(tokens[type])) 
        {
            cout << "Giving " << p->getName() << " a "
             << typenames[type] << " token." << endl;
            p->addToken(tokens[type].back());
            tokens[type].pop_back();
            // number of goods tokens in case of tie
            p->num_goods++; 
        }
        else
        {
            cout << "No more tokens left." << endl;
        }
    }
        
}


void Board::giveBonus(Player * p, int type) {

    srand(time(NULL));

    if (type == 3 ) {
        p->num_points += rand() % 3 + 1;
    } else if (type == 4) {
        p->num_points += rand() % 3 + 4;

    } else if (type >= 5) {
        p->num_points += rand() % 3 + 8;
    }
    // number of bonus tokens in case of tie
    p->num_bonus++;
}

void Board::giveCamelToken(Player * p) {
    p->num_points += 5;   
}

int Board::numCamels() {
    int cams = 0;
    for(int i = 0; i < 5; i++){ cams+=(theMarket.at(i).getType() == Ca);};
    return cams;
}

void Board::giveCardFromMarket(Player * p, int pos) {

    p->addCard(theMarket[pos-1]);
    theMarket.erase(theMarket.begin()+(pos-1));
}

void Board::giveCardToMarket(Player * p, int pos) {

    theMarket.push_back(*(p->hand()->begin()+(pos-1)));
    p->theHand.erase(p->theHand.begin()+(pos-1));

}

string Board::tok(int vect, int cnt) {

    int i;
    string ret = ""; 

    for (i=0; i< (signed)tokens[vect].size(); i++) { ret += "#"; }
    for (i=cnt- (signed) tokens[vect].size(); i>0; i--) { ret += " "; }

    return ret;

}

bool Board::roundFinished() {

    // If the pile has no more cards, round finished
    if (thePile.size() == 0) {
        cout << "Deck has no more cards; round done." << endl;
        return true;
    }
    
    int i, countZero = 0;
    
    // Go through each token pile and determine 
    // how many piles have zero tokens left
    // TODO check if the indices here are correct
    for (i=1; i<7; i++) {
        if (tokens[i].size() == 0) countZero++;
    } 
    
    // If three or more piles are empty, round finished
    if (countZero >= 3) {
        cout << "Three or more token piles are empty; round done." << endl;
        return true;
    }
    
    return false;

}

void Board::forceQuitRound() {
    thePile.clear();
}

void Board::clearBoard() {
    tokens.clear();
    thePile.clear();
    theMarket.clear();
}

void Board::print(Player * p1) {

    printf(
            "+-------------------+--------------------------------------------------+\n"     
            "|  Tokens:          |                                                  |\n"     
            "|                   |                +-----+                           |\n"    
            "| Di    %s       " "|                |  ?  | %-26s"                   "|\n"    
            "| Au    %s       " "|                +-----+                           |\n"    
            "| Ag    %s       " "|   +-----+        1     2     3     4     5       |\n"  
            "| Cl    %s     "   "|   |  ?  |      +---+ +---+ +---+ +---+ +---+     |\n"   
            "| Sp    %s     "   "|   |  ?  |      | %s| | %s| | %s| | %s| | %s|     |\n" 
            "| L     %s   "     "|   |  ?  |      | %s| | %s| | %s| | %s| | %s|     |\n"    
            "|                   |   +-----+      +---+ +---+ +---+ +---+ +---+     |\n"    
            "| B3    %s      "  "|     Deck                    Market               |\n"    
            "| B4    %s      "  "|                                                  |\n"    
            "| B5    %s      "  "|                +-----+                           |\n"    
            "|                   |                | % 2d  | %-26s"                  "|\n"   
            "| R: %02d \t"  "    |                +-----+                           |\n"    
            "|                   |                                                  |\n"     
            "+-------------------+--------------------------------------------------+\n",

            tok(Di, 5).c_str(), 
            "Opponent's herd", 
            tok(Au, 5).c_str(), tok(Ag, 5).c_str(),
            tok(Cl, 7).c_str(), tok(Sp, 7).c_str(), 

            //market (first char)
            (typeabvs[theMarket[0].getType()].substr(0,1) + " ").c_str(),
            (typeabvs[theMarket[1].getType()].substr(0,1) + " ").c_str(),
            (typeabvs[theMarket[2].getType()].substr(0,1) + " ").c_str(),
            (typeabvs[theMarket[3].getType()].substr(0,1) + " ").c_str(),
            (typeabvs[theMarket[4].getType()].substr(0,1) + " ").c_str(),

            tok(Le, 9).c_str(),

            //market (second char)
            (typeabvs[theMarket[0].getType()].substr(1,1) + " ").c_str(),
            (typeabvs[theMarket[1].getType()].substr(1,1) + " ").c_str(),
            (typeabvs[theMarket[2].getType()].substr(1,1) + " ").c_str(),
            (typeabvs[theMarket[3].getType()].substr(1,1) + " ").c_str(),
            (typeabvs[theMarket[4].getType()].substr(1,1) + " ").c_str(),

            tok(Bo3, 6).c_str(), tok(Bo4, 6).c_str(), tok(Bo5, 6).c_str(),

            p1->num_camels, (p1->name + "'s Herd").c_str(),
        
            p1->num_points

    );

    p1->printHand();

}

