#include "GUI.h"
#include <cstdlib>
#include <string>

using std::ofstream;
using std::ifstream;
using std::string;
using namespace Type;
GUI::GUI()
{
    fp = new ofstream;
}

void GUI::open()
{   
    //system("nohup python system.py 22 > & 1 &");
    //system("xdg-open http://ugradx.cs.jhu.edu:20022/");   
    return;
}

void GUI::update(Player * p)
{
    fp->open("templates/out.html");
    drawBoard(fp, p);
    fp->close();
}

void GUI::setBoard(Board * b)
{
    board = b;
}

void GUI::drawBoard(ofstream * fp, Player * p)
{
    *fp << "<div id=\"board\">" << endl;
    drawTokens(fp);
    drawMarket(fp);
    drawHand(fp, p);
    *fp << "</div>" << endl;

}

void GUI::drawMarket(ofstream * fp)
{
    *fp << "<div id=\"market\"> MARKET";
    *fp << "<div class=\"line\">";
    for(int i = 0; i < 5; i++) {
        string type = typeabvs[board->market()->at(i).getType()];
        *fp << "<div onclick = \"flag(this)\" id=\"card"<<i<<"\" class=\"card " << type << "\"></div>";
    }
    *fp << "<div id=\"deck" << (int) board->pile()->size() / 30 << "\"></div>";
    *fp << "</div>";

    *fp << "</div>" << endl;
}

void GUI::drawTokens(ofstream * fp)
{
    *fp << "<div id=\"tokens\"> TOKENS" << endl;
    for(int t = Le; t <=Bo5; t++)
    {
        string type =  typeabvs[t];
        *fp << "<div class=\"tokenHolder\"><div class=\"tokenIcon " << type << "\"></div>";
        for(int i = 0; i < (signed) board->tokens[t].size(); i++) {
            string type = typeabvs[board->tokens.at(t).at(i).getType()];
            *fp << "<div class=\"token " << type << "\"></div>";
        }
        *fp << "</div>";
    }
    *fp << "</div>";

}

void GUI::drawHand(ofstream *fp, Player * p)
{
    *fp << "<div id=\"hand\">" << p->getName() << endl; 
    *fp << "<div>";
    for(int i = 0; i < (signed) p->hand()->size(); i++) {
        string type = typeabvs[p->hand()->at(i).getType()];
        *fp << "<div onclick=\"flag(this)\" class=\"card " << type << "\" >"
               "</div>";
    }
    *fp << "</div>";
    *fp << "<div class=\"line\">";
    for(int i = 0; i < p->getCamels(); i++)
        *fp << "<div class=\"tokenIcon Ca\"></div>";

    *fp << "</div>" << endl;
    *fp << "<div class=\"line\">" << endl;
    *fp << "<div id=\"rupee\"></div>" << endl;
    *fp << "<div id=\"points\">" << std::to_string(p->getPoints()) << " RUPEES</div>" << endl;

    *fp << "</div></div>" << endl;
}
