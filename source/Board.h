
/*
 * The board holds the tokens, pile, and the market.
 * Responsible for distribution of cards and tokens to
 * players, as well as maintaining the market.
 *
 */

#ifndef BOARD_H
#define BOARD_H

#include "Card.h"
#include "Player.h"

#include <vector>
#include <string>
#include <iostream>

using std::vector;
using std::string;

class Board {
    friend class GUI;
    vector< vector<Token> > tokens;
    // Di: 5 5 5 7 7
    // Au: 5 5 5 6 6 
    // Ag: 5 5 5 5 5
    // Cl: 1 1 2 2 3 3 5
    // Sp: 1 1 2 2 3 3 5
    // Le: 1 1 1 1 1 1 2 3 4
    // Bonuses: 6 each

    vector<Card> thePile;

    vector<Card> theMarket;


public:

    //Initialize all tokens and deck
    Board(); 

    ~Board() {}

    void initTokens(); //create all tokens
    void initPile();   //create all cards


    //Accessors
    vector<Card> * pile();
    vector<Card> * market();
    Card draw();
    Token popToken(string);

    void deal(Player *, Player *);

    void giveToken(Player *, int, int);
    void giveBonus(Player *, int);
    void giveCamelToken(Player *);

    void giveCardFromMarket(Player *, int);
    void giveCardToMarket(Player *, int);
    void addToMarket(Card);
    int numCamels();

    void printMarket();

    void toString();
    string tok(int vect, int cnt);

    void print(Player *);
    
    bool roundFinished();

    // force quits the round, can use for valgrind
    void forceQuitRound();
    
    void clearBoard();
};



#endif
