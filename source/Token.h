
/*
 * Token class. Extends Piece.
 * Token adds a rupee value to each object
 */

#ifndef TOKEN_H
#define TOKEN_H

#include "Piece.h"

#include <string>

using std::string;

class Token : public Piece {

    //Type: diamond, gold, silver, cloth, spice,
    //camel, bonus3, bonus4, bonus5
    int value;

public:
    //Constructors
    Token();
    Token(int, int);

    //Destructor
    ~Token() {}

    //Accessors
    int getValue();

};

#endif
