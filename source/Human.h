
/* Human class extends Player.
 * Human is responsible for getting input from human players
 * and executing moves with this information.
 */

#ifndef HUMAN_H
#define HUMAN_H

#include "Player.h"
#include <string>

class Human : public Player {

public:

    Human(string);

    ~Human() {};

	int makeMove(Board *); 
    int makeMove(char, Board *);
    int takeA(Board *);
    int takeB(Board *);
    int takeC(Board *);
    int sellChoices(Board *);

};



#endif
