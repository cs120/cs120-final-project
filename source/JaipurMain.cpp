/*
Tiffany Chung   tchung12@jhu.edu
Gabriel Garcia  ggarci19@jhu.edu
Ryan Marren     rmarren1@jhu.edu
Jenny Wagner    jwagne47@jhu.edu

600.120 Intermediate Programming
Final Project: pg7c
*/

#include "Game.cpp"
#include <string>
#include <iostream>
#include <cstdlib>
#include <cassert>

using std::string;
using std::cout;
using std::endl;
using std::cin;
using namespace Type;


int main(void) {

    Game g;
    
    string p1, p2;
    char t1, t2;
    string cont;

    //Welcome message

    cout << 

        "      _       _                  "     << endl <<
        "     | |     (_)                 "     << endl <<
        "     | | __ _ _ _ __  _   _ _ __ "     << endl <<
        " _   | |/ _` | | '_ \\| | | | '__|"    << endl <<
        "| |__| | (_| | | |_) | |_| | |   "     << endl <<
        " \\____/ \\__,_|_| .__/ \\__,_|_|   "  << endl <<
        "               | |               "     << endl <<
        "               |_|               "     << endl << endl;

    //Get player information

    cout << "Enter player 1's name: ";
    cin >> p1;

    cout << "Is player 1 a human (a) or a computer (b)? ";
    cin >> t1;

    cout << "Enter player 2's name: ";
    cin >> p2;

    cout << "Is player 2 a human (a) or a computer (b)? ";
    cin >> t2;


    //Initialize players
    g.setPlayer1(t1, p1);
    g.setPlayer2(t2, p2);

    //Deal cards to players
    g.board()->deal(g.player1(), g.player2());


    int turn = 0, round = 1;
    char quit, option;
    bool quitoption = false;
    string roundwinner = "";

    cout << "Would you like the force quit option? (y/n)" << endl;
    cin >> option;
    if (option == 'y') {
        quitoption = true;
    }

    std::cin.ignore( std::numeric_limits <std::streamsize> ::max(), '\n' );
    cout << endl << "Press ENTER to start game." << endl;
    std::cin.ignore( std::numeric_limits <std::streamsize> ::max(), '\n' );
    g.openGUI(); 

    //Game Loop
    while (!g.gameFinished()) {

        cout << "ROUND " << round << endl;

        //Round loop
        while (!g.board()->roundFinished()) {

            Player * p; 

            //Figure out whose turn it is
            if (turn % 2 == 0) {
                p = g.player1();
            } else {
                p = g.player2();
            }

            cout << endl << p->getName() << "'s turn!" << endl;

            g.board()->print(p);

            g.updateHTML(p);
            while ( !(p->makeMove(g.board())) );
            g.updateHTML(p);

            g.board()->print(p);

            if (quitoption) {
                // fast way to force quit
                cout << "Press 'q' to exit round." << endl;
                cin >> quit;
                if (quit == 'q') {
                    g.board()->forceQuitRound();
                }
            }

            turn++;

        }
        
        cout << endl << (roundwinner = g.determineRoundWinner()) << " wins the round!" << endl;

        //Only initialize new round if game is not finished
        if (!g.gameFinished()) {

            g.newRound();

            //Initialize players
            g.setPlayer1(t1, p1);
            g.setPlayer2(t2, p2);

            //Make sure loser of round goes first next round
            if (roundwinner == g.player1()->getName()) {
                turn = 1;
            } else {
                turn = 0;
            }

            //Deal cards to players
            g.board()->deal(g.player1(), g.player2());

            if (quitoption) {
                // fast way to quit, change later
                cout << "Press 'q' to exit game." << endl;
                cin >> quit;
                if (quit == 'q') {
                    g.forceQuitGame();
                }
            }

            cout << "\nEnter any key to start next round." << endl;
            std::cin.ignore( std::numeric_limits <std::streamsize> ::max(), '\n' );
         }

    }

    cout << "\nCongratulations, " << g.determineGameWinner() << " wins the game!" << endl;

    cout << "\nGoodbye." << endl;


    return 1;

}

