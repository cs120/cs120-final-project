/*
Move class which abstracts the execution of moves to allow a computer
to use moves using AI, in addition to human players.


ABSTRACT BASE CLASS
*/


#ifndef MOVE_H
#define MOVE_H

class Move {

protected:

    Board * board;
    Player * player;

public:

    Move(Board *, Player *);
    virtual ~Move() {}
};


#endif
