#include "Sell.h"

#include <string>
#include <iostream>
#include <exception>

using std:: string;
using std::cout;
using std::endl;
using std::cin;
using namespace Type;

Sell::Sell(Board * b, Player * p) : Move(b, p) { }


int Sell::sell(vector<int> toSell) {    
    // check if valid first
    if(!validSell(toSell)) { return 0;}

    int type = (player->hand()->at(toSell.at(0)-1)).getType();

    // remove the cards from player's hand
    std::sort(toSell.begin(), toSell.end());
    for (int i = toSell.size()-1; i >= 0; i--) {
        player->removeCard(toSell.at(i) - 1);
    }

    //give tokens to the player
    board->giveToken(player, type, toSell.size());
    
    if((signed) toSell.size() == 3) {
        board->giveToken(player, Bo3, 1);
    } else if ((signed) toSell.size() == 4) {
        board->giveToken(player, Bo4, 1);
    } else if ((signed) toSell.size() > 4){
        board->giveToken(player, Bo5, 1);
    }

    return 1;
}

// predicates for validSell
bool isSilver (int i){ return i == Ag;}
bool isGold   (int i){ return i == Au;}
bool isDiamond(int i){ return i == Di;}

bool Sell::validSell(vector<int> toSell) {
    int i;

    // check if they are selling 0 cards
    if(toSell.size() == 0){
        cout << "INVALID: Can't sell 0 cards!" << endl;
        return 0;
    }

    // check if the input actually made sense
    for(i=0; i< (signed)toSell.size(); i++) {
        if(toSell.at(i) < 0 || toSell.at(i) > (signed) player->hand()->size()){
            cout << "INVALID: You don't have a card # " << std::to_string(toSell.at(i)) << endl;
            return 0;
        }
    }

    // check if they are selling more than 1 type of card
    vector<int> sellType(toSell.size());
    vector<int> unique(toSell.size());
    vector<int>::iterator it;
    for(i=0; i<(signed)toSell.size(); i++) { sellType[i] = (player->hand()->at(toSell.at(i)-1).getType());}
    it = unique_copy(sellType.begin(), sellType.end(), unique.begin());
    unique.resize(it-unique.begin());
    if(unique.size() > 1){
        cout << "INVALID: Only one type of card can be sold."<< endl;
        return 0;   
    }

    // check if they are selling less than 2 of the expensive ones
    int count = 0;
    count = count_if(sellType.begin(), sellType.end(), isSilver);
    if(count == 1){ cout << "INVALID: Must sell 2+ Silver."  << endl; return 0;}
    count = count_if(sellType.begin(), sellType.end(), isGold);
    if(count == 1){ cout << "INVALID: Must sell 2+ Gold."    << endl; return 0;}
    count = count_if(sellType.begin(), sellType.end(), isDiamond);
    if(count == 1){ cout << "INVALID: Must sell 2+ Diamond." << endl; return 0;}
    
    return 1;
}

