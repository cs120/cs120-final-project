#include "Game.cpp"
#include <string>
#include <iostream>
#include <cstdlib>
#include <cassert>
#include <exception>

using std::string;
using std::cout;
using std::endl;
using namespace Type;

void testGame(Game *, char, char);
void testBoard(Game *);
void testPiece();
void testToken();
void testCard();
void testPlayer(Game *);
void testHuman(Game *);
void testComputer(Game *);

void testComputervsComputer(Game *);
void testHumanvsHuman(Game *);
void testEndGame(Game *);
void testExceptions(Game *);


int main() {

    // human-human games
/*    Game hhg1;
    cout << "Testing Human vs. Human..." << endl;
    testHumanvsHuman(&hhg1);
    testEndGame(&hhg1);

    Game hhg2;
    testHuman(&hhg2);
    testEndGame(&hhg2);*/

    // computer-computer games
    Game ccg2;
    testComputer(&ccg2);
    testExceptions(&ccg2);
    testEndGame(&ccg2);

    Game ccg1;    
    cout << "Testing Computer vs. Computer..." << endl;
    testComputervsComputer(&ccg1);

    // testing other methods
    Game tg;
    cout << "\n\nSpecific testing..." << endl;
    testGame(&tg, 'b', 'b');
    testPlayer(&tg);
    testBoard(&tg);
    testPiece();
    testToken();
    testCard();
    testEndGame(&tg);


    cout << "\n\nALL TESTS PASSED." << endl;

    return 1;
}


void testGame(Game * g, char p1, char p2) {

    //initialize player 1 as human
    g->setPlayer1(p1, "apple");

    //initialize player 2 as human
    g->setPlayer2(p2, "banana");

}


void testBoard(Game * g) {

    Board b = *(g->board());

    b.deal(g->player1(), g->player2());

    assert(b.market()->size() == 5);
    assert(b.pile()->size() == 55-15);

}


void testPiece() {

    Piece p = Piece(6);
    assert(6 == p.getType());

    Piece q = Piece(0);
    assert(0 == q.getType());

}



void testToken() {
    Token t = Token(Di, 4);
    assert(6 == t.getType());
    assert(4 == t.getValue());

    Token u = Token();
    assert(0 == u.getType());
    assert(-1 == u.getValue());

    // TODO: test bonus tokens

}


void testCard() {
    Card c = Card(3);
    Card d = Card(2);
    assert(d < c);

}


void testPlayer(Game * g) {

    assert(g->player1()->getName() == "apple");
    assert(g->player2()->getName() == "banana");
    assert(g->player1()->getPoints() == 0);
    assert(g->player2()->getPoints() == 0);
    assert(g->player1()->getCamels() == 0);
    assert(g->player2()->getCamels() == 0);

}

// moves are implicitly tested
void testHuman(Game * g) {
    
    g->setPlayer1('a', "apple");
    g->setPlayer2('a', "orange");

    g->board()->deal(g->player1(), g->player2());
    
    g->board()->print(g->player1());
    g->player1()->makeMove(g->board());

    g->board()->print(g->player2());
    g->player2()->makeMove(g->board());
    g->board()->forceQuitRound();

}

// moves are implicitly tested by computer's choices
void testComputer(Game * g) {

    g->setPlayer1('b', "apple");
    g->setPlayer2('b', "orange");

    g->board()->deal(g->player1(), g->player2());

    assert(g->player1()->takeC(g->board()) == 1);
    assert(g->player1()->takeA(g->board()) <= 1);
    assert(g->player1()->takeB(g->board()) <= 1);
    assert(g->player1()->sellChoices(g->board()) <= 1);
    g->board()->forceQuitRound();

}

// automated computer-computer game
void testComputervsComputer(Game * g) {

    //Initialize players
    g->setPlayer1('b', "apple");
    g->setPlayer2('b', "orange");

    //Deal cards to players
    g->board()->deal(g->player1(), g->player2());

    int turn = 0;
    while (!g->gameFinished()) {

        //Round loop
        while (!g->board()->roundFinished()) {

            Player * p;

            //Figure out whose turn it is
            if (turn % 2 == 0) {
                p = g->player1();
            } else {
                p = g->player2();
            }

            cout << endl << p->getName() << "'s turn!" << endl;
            g->board()->print(p);

            while ( !(p->makeMove(g->board())) );

            g->board()->print(p);
            turn++;
        }
        g->forceQuitGame();
    }
    cout << "Game finished!" << endl;

}

void testHumanvsHuman(Game * g) {
    //Initialize players
    g->setPlayer1('a', "apple");
    g->setPlayer2('a', "orange");

    //Deal cards to players
    g->board()->deal(g->player1(), g->player2());

    int turn = 0;
    char quit;
    while (!g->gameFinished()) {

        //Round loop
        while (!g->board()->roundFinished()) {

            Player * p;

            //Figure out whose turn it is
            if (turn % 2 == 0) {
                p = g->player1();
            } else {
                p = g->player2();
            }

            cout << endl << p->getName() << "'s turn!" << endl;
            g->board()->print(p);

            while ( !(p->makeMove(g->board())) );

            g->board()->print(p);
            turn++;

            cout << "Press 'q' to exit round." << endl;
            cin >> quit;
            if (quit == 'q') {
                g->board()->forceQuitRound();
            }
        }
        g->forceQuitGame();
    }
    cout << "Game finished!" << endl;
}


// ends the game neatly, calls destructors
void testEndGame(Game * g) {
    
    g->forceQuitGame();
}

void testExceptions(Game * g) {
    try {
        g->setPlayer2('t', "oranges");
    }
    catch(std::invalid_argument & e4) {
        cout << "invalid_argument exception caught." << endl;
        cout << e4.what() << endl;
    }

    try {
        g->board()->deal(g->player1(), g->player2());
    }
    catch(std::range_error & e1) {
        cout << "range_error exception caught." << endl;
        cout << e1.what() << endl;
    }

    try {
        for (int i = 0; i < 100; i++) {
            (g->board()->draw());
        }
    }
    catch(std::range_error & e2) {
        cout << "range_error exception caught." << endl;
        cout << e2.what() << endl;
    }
}