#include "Player.h"

#include <string>
#include <iostream>
#include <vector>
#include <exception>
#include <algorithm>

using std::string;
using std::cout;
using std::endl;
using std::vector;
using namespace Type;

Player::Player(string s) {

    // cout << "Creating player ... " << endl;

    name = s;

    num_points = 0;
    num_camels = 0;
    num_cards = 0;
    num_bonus = 0;
    num_goods = 0;
    num_seals = 0;
}


string Player::getName() const {
    return name;
}


int Player::getPoints() const {
    return num_points;
}

vector<Card> * Player::hand() {
    return &theHand;
}

int Player::getCamels() const{
    return num_camels;
}

void Player::printHand() {

    int i;

    cout << name << "'s hand: " << endl;
    for(i=0; i<(signed)theHand.size(); i++) { printf("  %d   ", i+1); } cout << endl;
    for(i=0; i<(signed)theHand.size(); i++) { printf("+---+ "); } cout << endl;
    for(i=0; i<(signed)theHand.size(); i++) { printf("| %s| ",
         (typeabvs[theHand.at(i).getType()].substr(0,1) + " ").c_str());} cout << endl;
    for(i=0; i<(signed)theHand.size(); i++) { printf("| %s| ", 
         (typeabvs[theHand.at(i).getType()].substr(1,1) + " ").c_str());} cout << endl;
    for(i=0; i<(signed)theHand.size(); i++) { printf("+---+ ");} cout << endl;

    cout << endl << endl;
}




void Player::addCard(Card c) {

    if (c.getType() == Ca) {
        num_camels++;
        return;
    }

    if (theHand.size() > 7) {
        throw std::range_error("ERROR: Hand Overflow.");
    }

    theHand.push_back(c);

    sort(theHand.begin(), theHand.end());


}

void Player::removeCard(int i) {
    theHand.erase(theHand.begin() + i);
}
void Player::addToken(Token t) {

    tokens.push_back(t);
    num_points = 0;
    for(vector<Token>::iterator i=tokens.begin(); i!=tokens.end(); i++)
        num_points += (*i).getValue();


}

void Player::playCamel() {
    if(num_camels > 0) {
        num_camels--;
    } else {
        throw std::invalid_argument("ERROR: Camel played when 0 camels in herd.");
    }
}
    


