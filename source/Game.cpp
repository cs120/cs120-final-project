#include "Game.h"
#include "Board.cpp"
#include "GUI.cpp"
#include "Player.cpp"
#include "Human.cpp"
#include "Computer.cpp"
#include "Take.cpp"
#include "Sell.cpp"


#include <string>
#include <iostream>
#include <cstdlib>
#include <exception>

using std::string;
using std::cout;
using std::endl;

Game::Game() {
    GUI * g = new GUI;
    g->setBoard(&b);
    gui = g;
}

void Game::updateHTML(Player * p) {
    gui->update(p);
}

void Game::openGUI() {
    gui->open();
}

Human * Game::initHuman(string name) {

    return new Human(name);

}

Computer * Game::initComputer(string name) {

    return new Computer(name);

}

// Takes 'a' or 'b' as input */
Player * Game::initPlayer(char choice, string name) {

    if (choice == 'a') return initHuman(name);
    else if (choice =='b') return initComputer(name);

    throw std::invalid_argument("ERROR: Invalid Option");


}

Player * Game::player1() {
    return p1;
}

Player * Game::player2() {
    return p2;
}

Board * Game::board() {
    return &b;
}



void Game::setPlayer1(char choice, string name) {

    p1 = initPlayer(choice, name);
}


void Game::setPlayer2(char choice, string name) {
    p2 = initPlayer(choice, name);
}
    
bool Game::gameFinished() {
    
    if (p1Wins < 2 && p2Wins < 2) return false;
    
    return true;
}   

string Game::determineRoundWinner() {
    
    int p1pts = p1->getPoints();
    int p2pts = p2->getPoints();

    // If tie round
    if (p1pts == p2pts) {
        // throw std::logic_error("WE HAVENT CODED WHAT TO DO IN TIE GAME!!!");
        cout << "TIE GAME." << endl;
        return tieBreaker();
    }
    
    // If player 1 wins
    if (p1pts > p2pts) {
        p1Wins++;
        return p1->getName();
    }
    
    // If player 2 wins
    p2Wins++;
    return p2->getName();
}

string Game::tieBreaker() {

    if (p1->getPoints() == 0 && p2->getPoints() == 0) {
        return "Tie at 0-0. Neither";
    }
    
    cout << "Winner determined by bonus tokens." << endl;

    // in case of tie, player with most bonus tokens is winner
    int p1bonus = p1->getBonusTok();
    int p2bonus = p2->getBonusTok();
    
    int p1goods = p1->getGoodsTok();
    int p2goods = p2->getGoodsTok();    
    
    if (p1bonus > p2bonus) {
        p1Wins++;
        return p1->getName();
    }
    else {
        p2Wins++;
        return p2->getName();
    }

    // if still a tie, player with most goods tokens wins
    cout << "Tie again, winner determined by goods tokens." << endl;

    if (p1bonus == p2bonus) {
        if (p1goods > p2goods) {
            p1Wins++;
            return p1->getName();
        }
        else {
            p2Wins++;
            return p2->getName();
        }
    }

}


string Game::determineGameWinner() {

  	Player * winner;
    
    (p1Wins > p2Wins) ? (winner = p1) : (winner = p2);
    
    return winner->getName();


}

void Game::newRound() {

    b.clearBoard();
    b = Board();
    delete p1;
    delete p2;

}


void Game::forceQuitGame() {
    cout << "FORCE QUIT GAME" << endl;
    p1Wins = 3;
    p2Wins = 0;
}
